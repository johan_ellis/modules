# SASFIN Front-end development

## Dependencies
* Node
* Gulp cli
* Bower
* Git

## Installing dependencies
```
npm install -g gulp-cli
npm install -g bower
```

## Running project
```
gulp serve # runs local server for development and watching files
gulp # builds project into dist folder
gulp serve:dist # runs local server of production version
```

## Further instructions
index.html - contains SASFIN ui-kit files
modules.html - contains all modules for page building
