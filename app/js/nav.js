$(document).ready(function(){
	(function(){

		/*
		 * Class SasNav
		 */
		var SasNav = (function() {

			// Constructor
			function SasNav() {}
			
			/* ----------------------------------------
			Properties
			---------------------------------------- */
			// Threshold at which we switch to minimized menu format
			// For best results this should be set the a minimum of the height 
			// of the maximised menu element. This is done because the maximised version
			// of the menu is hidden using the visibility rule to keep it in the DOM tree
			// so as not to screw with the scroll position
			SasNav.prototype.menuScrollThreshold = 100; 
			SasNav.prototype.menuIsMinimised = false;
			SasNav.prototype.cache = {};
			// SasNav.prototype.sayName= function(){alert(this.name)}


			/* ----------------------------------------
			Methods
			---------------------------------------- */
			
			/*
			 * Search bar behaviour
			 */	
			SasNav.prototype.showSearchBar=function(){
				var searchBar = $('.ss-main-nav .search-module')
				searchBar.removeClass('hide')
			}
			SasNav.prototype.hideSearchBar=function(){
				var searchBar = $('.ss-main-nav .search-module')
				searchBar.addClass('hide')
			}
			SasNav.prototype.toggleSearchBar=function(){
				/*
					First hide all currently showing submenus
				*/
				// this.hidePrimarySubmenu();

				// var searchBar = $('#ss-top-nav #ss-search-wrapper')
				var searchBar = this.cache['searchBar'];
				var mainNav = this.cache['mainNav']
				var blocker = this.cache['blocker']

				if(searchBar.hasClass('ss-offcanvas')){ /*SHOW*/
					// Handle the search bar
					searchBar.removeClass('ss-offcanvas')
					searchBar.css('top','0');

					// Handle the shift in the menu
					mainNav.css('top','43px') // move down the height of the search bar

					// Manage the blocker
					blocker.css('display','block')
					blocker.off() /// Reset any previous listeners
					blocker.click(this.toggleSearchBar.bind(this))
				}else{/*HIDE*/
					// Handle the search bar
					searchBar.addClass('ss-offcanvas')
					searchBar.css('top','-1000px') /*Make this a sane value*/

					// Handle the shift in the menu
					mainNav.css('top','0') // Reset to top

					// Manage the blocker
					blocker.css('display','none')
					blocker.off()
				}
			}
			/*
			 * Login group behaviour
			 */	
			SasNav.prototype.showLoginGroup=function(){
				// console.log('showLoginGroup');


				this.resetNav();
				/*
					Show all the list items in the login group
				*/
				var loginGroupDesktopNav = $('.login-dropdown-group')
				,firstChild = $(loginGroupDesktopNav.children()[0])
				// Show all items
				loginGroupDesktopNav.children().removeClass('hide');
				firstChild.addClass('is-open')

				// loginGroupDesktopNav.on('mouseleave', this.hideLoginGroup.bind(this));
			}
			SasNav.prototype.showLoginGroupAndListenForMouseOut=function(){
				/*
					First hide all currently showing submenus
				*/
				this.hidePrimarySubmenu();

				/*
					Show all the list items in the login group
				*/
				var loginGroupDesktopNav = $('.login-dropdown-group')
				,firstChild = $(loginGroupDesktopNav.children()[0])
				// Show all items
				loginGroupDesktopNav.children().removeClass('hide');
				firstChild.addClass('is-open')

				// remove all listeners from the login group
				// and register a listener for the mouseout event
				loginGroupDesktopNav.off()
				loginGroupDesktopNav.on('mouseleave',this.onLoginGroupOut.bind(this));
			}
			SasNav.prototype.onLoginGroupOut=function(){
				/*
					Show all the list items in the login group
				*/
				var loginGroupDesktopNav = $('.login-dropdown-group')
				,firstChild = $(loginGroupDesktopNav.children()[0])
				// Hide all except the first li
				loginGroupDesktopNav.children().addClass('hide');
				firstChild.removeClass('is-open hide')

				// remove all listeners from the login group
				// and register a listener for the mouseover event
				loginGroupDesktopNav.off()
				loginGroupDesktopNav.on('mouseover',this.showLoginGroupAndListenForMouseOut.bind(this)); // used for desktop where mouseover is enabled
			}

			SasNav.prototype.hideLoginGroupDesktop=function(){
				this.hideLoginGroup('desktop')
			}
			SasNav.prototype.hideLoginGroup=function(device){
				/*
					Hide all the list items in the login group.
				*/
				if(device && device === 'desktop'){

					var loginGroupDesktopNav = this.cache['loginGroupDesktopNav']
					,firstChild = $(loginGroupDesktopNav.children()[0])
					
					/*
						Inspect the first li to see whether it hasClass 'is-open'
					*/
					if(firstChild.hasClass('is-open')){
						// Hide all except the first li
						loginGroupDesktopNav.children().addClass('hide');
						firstChild.removeClass('is-open hide')
					}
				}else if(device && device === 'mobile'){

					var mobileNavLoginButton = this.cache['mobileNavLoginButton']
					, mobileNavLoginDropdown = this.cache['mobileNavLoginDropdown']

					/*
						Inspect the anchor to 
					*/
					if(mobileNavLoginButton.hasClass('is-open')){
						// Hide all except the first li
						mobileNavLoginDropdown.addClass('hide');
						mobileNavLoginButton.removeClass('is-open hide')
					}
				}
			}


			SasNav.prototype.toggleLoginGroupDesktop=function(){
				console.log('toggleLoginGroupDesktop');

				this.toggleLoginGroup('desktop');
			}
			SasNav.prototype.toggleLoginGroupMobile=function(){
				this.toggleLoginGroup('mobile');
			}


			SasNav.prototype.toggleLoginGroup=function(device){
				if(device && device === 'desktop'){
					/*
						First hide all currently showing submenus
					*/
					this.resetNav();

					var loginGroupDesktopNav = this.cache['loginGroupDesktopNav']
					,firstChild = $(loginGroupDesktopNav.children()[0])
					
					/*
						Inspect the first li to see whether it hasClass 'is-open'
					*/
					if(firstChild.hasClass('is-open')){
						// Hide all except the first li
						loginGroupDesktopNav.children().addClass('hide');
						firstChild.removeClass('is-open hide')
					}else{
						// Show all items
						loginGroupDesktopNav.children().removeClass('hide');
						firstChild.addClass('is-open')
					}
				}else if(device && device === 'mobile'){

					console.log('mobile login group clicked');

					this.closeMobileMenu()

					var mobileNavLoginButton = this.cache['mobileNavLoginButton']
					, mobileNavLoginDropdown = this.cache['mobileNavLoginDropdown']

					/*
						Inspect the anchor to 
					*/
					if(mobileNavLoginButton.hasClass('is-open')){
						// Hide all except the first li
						mobileNavLoginDropdown.addClass('hide');
						mobileNavLoginButton.removeClass('is-open hide')
					}else{
						// Show all items
						mobileNavLoginDropdown.removeClass('hide');
						mobileNavLoginButton.addClass('is-open')
					}
				}
			}




			SasNav.prototype.closeMobileMenu=function(){
				var mobileMenuButtonWrapper = this.cache['mobileMenuButtonWrapper'];

				if(mobileMenuButtonWrapper.hasClass('is-open')){
					mobileMenuButtonWrapper.removeClass('is-open')
				}
				
				/*
					Hide the burger menu and close button icons	
				*/
				var anchors = $(mobileMenuButtonWrapper).find('a');
				$.each(anchors, function(){
					var t = $(this)
					if(!t.hasClass('hide')){
						t.addClass('hide')
					}

					// BUT show the burger menu icon
					if(t.children('i').hasClass('icon-menu')){
						t.removeClass('hide')
					}
				})
				/*
					Manage the visibility state of the submenu (hide it)
				*/
				var submenuWrapper = this.cache['submenuWrapperMobile']
				
				if(!submenuWrapper.hasClass('hide')){
					submenuWrapper.addClass('hide')
				}
			}

			SasNav.prototype.toggleMobileMenu=function(){

				this.hideLoginGroup('mobile');

				var mobileMenuButtonWrapper = this.cache['mobileMenuButtonWrapper'];

				if(mobileMenuButtonWrapper.hasClass('is-open')){
					mobileMenuButtonWrapper.removeClass('is-open')
				}else{
					mobileMenuButtonWrapper.addClass('is-open')
				}

				/*
					Toggle the burger menu OR close button icons	
				*/
				var anchors = $(mobileMenuButtonWrapper).find('a');
				$.each(anchors, function(){
					var t = $(this)
					if(t.hasClass('hide')){
						t.removeClass('hide')
					}else{
						t.addClass('hide')
					}
				})

				/*
					Manage the visibility state of the submenu
				*/
				var submenuWrapper = this.cache['submenuWrapperMobile']
				if(submenuWrapper.hasClass('hide')){
					submenuWrapper.removeClass('hide')
				}else{
					submenuWrapper.addClass('hide')
				}
			}

			/*
			 * Primary links and dropdown menu behaviour
			 */
			SasNav.prototype.showBusinessMenuForDesktop=function(e){
				this._showMenuForDesktop('business');
			}
			SasNav.prototype.showWealthMenuForDesktop=function(e){
				this._showMenuForDesktop('wealth');
			}
			SasNav.prototype.showBankingMenuForDesktop=function(e){
				this._showMenuForDesktop('banking');
			}
			SasNav.prototype._showMenuForDesktop=function(menuId){

				this.resetNav();

				/*
					Show the submenu wrapper
				*/
				var submenuWrapper = this.cache['submenuWrapper'];
				if(submenuWrapper.hasClass('hide')){
					submenuWrapper.removeClass('hide')
				}

				/*
				Hide all submenu contexts
				*/
				var allSubmenuContexts = submenuWrapper.find('div[data-primary-link]');
				allSubmenuContexts.each(function(){
					$(this).addClass('hide')
				})

				/*
					Show the respective subnav details
					submenuWrapper.find('data-submenu-type=menuId')
				*/
				var submenuToShow = submenuWrapper.find('div[data-primary-link=\''+menuId+'\']')
				if(submenuToShow.hasClass('hide')){
					submenuToShow.removeClass('hide')
				}

			}
			SasNav.prototype.hidePrimarySubmenu = function() {
				// Hide the current primary nav dropdown
				var dropdowns = $('.ss-main-nav').find('[data-primary-menu]').addClass('hide')

				// remove the up arrow that is below the primary menu link
				var primaryNavLinks = $('.ss-main-nav').find('[data-primary-link]').removeClass('is-active')
			};

			SasNav.prototype.resetNav = function() {
				/*
					** PSUEDO CODE **
					Hide login (desktop and mobile)		
					Hide submenu
				*/
				/*
					Hide the submenu wrapper
				*/
				var submenuWrapper = this.cache['submenuWrapper'];
				if(!submenuWrapper.hasClass('hide')){
					submenuWrapper.addClass('hide')
				}


				/*
					Hide login dropdown group for desktop
				*/
				/*var loginGroupDesktopNav = this.cache['loginGroupDesktopNav']
				,firstChild = $(loginGroupDesktopNav.children()[0])
				
				if(firstChild.hasClass("is-open")){
					// Hide all except the first li
					loginGroupDesktopNav.children().addClass("hide");
					firstChild.removeClass('is-open hide')
				}*/


				/*
					Hide login dropdown group for mobile
				*/
				var mobileNavLoginButton = this.cache['mobileNavLoginButton']
				, mobileNavLoginDropdown = this.cache['mobileNavLoginDropdown']

				/*
					Inspect the anchor to 
				*/
				if(mobileNavLoginButton.hasClass('is-open')){
					// Hide all except the first li
					mobileNavLoginDropdown.addClass('hide');
					mobileNavLoginButton.removeClass('is-open hide')
				}

				this.closeMobileMenu()

			};

			/*
			 * Scroll menu behaviour
			 */
			SasNav.prototype.onWindowScroll = function() {
				// TODO: Optimise this so that jQuery is not looking
				// for the top nav element on every scroll movement
				//...
				
				var scrollPos = $(window).scrollTop().valueOf();
				
				var topNav;

				if(scrollPos > this.menuScrollThreshold){
					topNav = $('div#ss-top-nav');
					if(!topNav.hasClass('minimised')){
						topNav.addClass('minimised')

						/*
							Because we are showing different verions of the desktop menu depending on the scroll
							position, we need to update the pointer to point to the correct login dropdown ul
						*/
						this.cache['loginGroupDesktopNav'].off();
						this.cache['loginGroupDesktopNav'] = this.cache['loginGroupDesktopNavMinimised'];
						this.cache['loginGroupDesktopNav'].on('click',this.toggleLoginGroupDesktop.bind(this));
					}

				}else{
					topNav = $('div#ss-top-nav');
					if(topNav.hasClass('minimised')){
						topNav.removeClass('minimised')

						/*
							Because we are showing different verions of the desktop menu depending on the scroll
							position, we need to update the pointer to point to the correct login dropdown ul
						*/
						this.cache['loginGroupDesktopNav'].off();
						this.cache['loginGroupDesktopNav'] = this.cache['loginGroupDesktopNavMaximised'];
						this.cache['loginGroupDesktopNav'].on('click',this.toggleLoginGroupDesktop.bind(this));
					}

				}
			};

			/*
			 * Wire up behaviour 
			 */			
			SasNav.prototype.bootstrap= function(){
				
				// var searchButtonMainNav = $('.ss-secondary-nav-right a')
				// , loginGroupDesktopNav = $('.login-dropdown-group')
				// , primaryDesktopLinkForBusiness = $("ul.ss-primary-nav-button-group li[data-primary-link='business']")
				// , primaryDesktopLinkForWealth = $("ul.ss-primary-nav-button-group li[data-primary-link='wealth']")
				// , primaryDesktopLinkForBanking = $("ul.ss-primary-nav-button-group li[data-primary-link='banking']")

				// var searchButtonMainNav = $('#ss-top-nav #ss-maximised-wrapper .nav-right .secondary-nav .secondary-nav-search-group a')

				this.cache['searchButtonMainNav'] = $('#ss-top-nav #ss-maximised-wrapper .nav-right .secondary-nav .secondary-nav-search-group a');
				this.cache['searchButtonMainNavMinimised'] = $('#ss-top-nav #ss-minimised-wrapper a.ss-search-button');
				this.cache['searchBar'] = $('#ss-search-wrapper');
				this.cache['mainNav'] = $('#ss-top-nav');
				this.cache['blocker'] = $('#blocker');
				this.cache['burgerMenuButton'] = $('#ss-top-nav #ss-mobile .nav-right .menu-toggle-group a');
				this.cache['mobileMenuButtonWrapper'] = $('#ss-top-nav #ss-mobile .nav-right .menu-toggle-group');
				this.cache['submenuWrapper'] = $('#ss-top-nav #ss-submenu-wrapper')
				this.cache['submenuWrapperMobile'] = $('#ss-submenu-wrapper-mobile')
				this.cache['primaryLinksDesktop'] = $('#ss-top-nav #ss-maximised-wrapper .nav-right .primary-nav .primary-nav-link-group .primary-nav-list');
				this.cache['businessLinkDesktop'] = $(this.cache['primaryLinksDesktop'].children()[0])
				this.cache['wealthLinkDesktop'] = $(this.cache['primaryLinksDesktop'].children()[1])
				this.cache['bankingLinkDesktop'] = $(this.cache['primaryLinksDesktop'].children()[2])



				// login dropdown

				this.cache['loginGroupDesktopNavMaximised'] = $('#ss-top-nav #ss-maximised-wrapper .nav-right .primary-nav .primary-nav-link-group .login-dropdown-group');
				this.cache['loginGroupDesktopNavMinimised'] = $('#ss-top-nav #ss-minimised-wrapper .nav-right .primary-nav .primary-nav-link-group .login-dropdown-group');
				this.cache['loginGroupDesktopNav'] = this.cache['loginGroupDesktopNavMaximised'];
				this.cache['mobileNavLoginButton'] = $('#ss-top-nav #ss-mobile .nav-right .login-group a');
				this.cache['mobileNavLoginDropdown'] = $('#ss-top-nav #ss-mobile .nav-right .login-group ul.login-dropdown-group');


				// Click behaviours
				// this.cache['searchButtonMainNav'].on('click',this.toggleSearchBar.bind(this));
				// this.cache['searchButtonMainNavMinimised'].on('click',this.toggleSearchBar.bind(this));


				this.cache['burgerMenuButton'].on('click',this.toggleMobileMenu.bind(this));
				// var self = this;
				// $( ".ss-search-button" ).each(function() {
				// 		$( this ).on('click',self.toggleSearchBar.bind(self));
				// });

				this.cache['submenuWrapper'].on('mouseleave',this.resetNav.bind(this)); // For desktop


				/*
					Interaction with the Login/Register dropdown
				*/
				// loginGroupDesktopNav.on('click',this.toggleLoginGroup); // used for mobile where no rollover exists
				// loginGroupDesktopNav.on('mouseover', this.showLoginGroupAndListenForMouseOut.bind(this) ); // used for desktop where mouseover is enabled
				this.cache['loginGroupDesktopNav'].on('click',this.showLoginGroup.bind(this));
				this.cache['loginGroupDesktopNav'].on('mouseleave',this.hideLoginGroupDesktop.bind(this));
				this.cache['mobileNavLoginButton'].on('click',this.toggleLoginGroupMobile.bind(this));
				this.cache['mobileNavLoginDropdown'].on('click',this.toggleLoginGroupMobile.bind(this));

				/*
					Interaction with the primary navigation links
				*/
				this.cache['businessLinkDesktop'].on('click', this.showBusinessMenuForDesktop.bind(this));
				this.cache['wealthLinkDesktop'].on('click', this.showWealthMenuForDesktop.bind(this));
				this.cache['bankingLinkDesktop'].on('click', this.showBankingMenuForDesktop.bind(this));


				/*
					Scroll behaviour for the main menu listener
				*/
				$(window).scroll(this.onWindowScroll.bind(this));

			};
			return SasNav
		})();


		/* ----------------------------------------
		Implementation
		---------------------------------------- */
		var sasnav = new SasNav()
		sasnav.bootstrap()

	}).call(this)
});

