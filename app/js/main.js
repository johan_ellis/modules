var xraySlider;
var xrayCreated = false;
var rollModules = '.content-module-beta, .content-module-charlie, .content-module-foxtrot';

$(document).ready(function($){

  $(".content-module-alpha, .content-module-beta, .content-module-charlie, .content-module-foxtrot").click(function(e) {
    e.preventDefault();
    window.location = $(this).data('link');
  return false;
  });
  function mouseEvents() {
    if (Foundation.MediaQuery.current != 'small' && Foundation.MediaQuery.current != 'medium') {
      $('.whiskey-row').removeClass('expanded');
      $(rollModules).mouseenter(function() {
        var el = $(this);
        if (el.find('.btn-secondary').css('opacity') != 1) {
          el.find('.text-content').animate({
            top: -30
          }, 150);

          el.find('.btn-secondary').animate({
            opacity: 1
          }, 200);
        }
      });

      $(rollModules).mouseleave(function() {
        var el = $(this);

        if (el.find('.btn-secondary').css('opacity') != 0) {
          el.find('.btn-secondary').animate({
            opacity: 0
          }, 150);

          el.find('.text-content').animate({
            top: 0
          }, 200);
        }
      });

      $('.content-module-romeo').mouseenter(function() {
        var el = $(this);
        var endIcon = el.find('.end-icon');
        var startIcon = el.find('.start-icon');
        var heading = el.find('h4');

        if (endIcon.css('opacity') != 1) {
          endIcon.animate({
            opacity: 1,
            right: 0
          }, 200);

          heading.animate({
            left: 0
          }, 200);

          startIcon.animate({
            left: -32,
            opacity: 0
          }, 50);
        }
      });

      $('.content-module-romeo').mouseleave(function() {
        var el = $(this);
        var endIcon = el.find('.end-icon');
        var startIcon = el.find('.start-icon');
        var heading = el.find('h4');

        if (endIcon.css('opacity') != 0) {
          endIcon.animate({
            opacity: 0,
            right: -32
          }, 200);

          heading.animate({
            left: 42
          }, 200);

          startIcon.delay(100).animate({
            left: 0,
            opacity: 1
          }, 200);
        }
      });

      $('.juliet-card').mouseenter(function() {
        var el = $(this);
        el.find('juliet-content').animate({
          'padding-top': 30
        }, 200);
      });

      $('.juliet-card').mouseleave(function() {
        var el = $(this);
        el.find('juliet-content').animate({
          'padding-top': 40
        }, 200);
      });

      $('.zulu-card').mouseenter(function() {
        var el = $(this);
        el.find('.zulu-content').animate({
          'padding-top': 30
        }, 200);
      });

      $('.zulu-card').mouseleave(function() {
        var el = $(this);
        el.find('.zulu-content').animate({
          'padding-top': 40
        }, 200);
      });

    } else {
      $('.whiskey-row').addClass('expanded');
      $(rollModules).unbind('mouseleave mouseenter');
      $('.content-module-romeo').unbind('mouseleave mouseenter');
      $('.zulu-card').unbind('mouseleave mouseenter');
      $('.juliet-card').unbind('mouseleave mouseenter');
    }
  }
  function xrayFunctionality() {
    var obj = $('.content-module-xray');
    if (obj.length > 0) {
      var breakpoints = {
        default: {
          cards: 4
        },
        medium: {
          cards: 2,
          size: 1024
        },
        small: {
          cards: 1,
          size: 480
        }
      };
      var stageWidth = window.outerWidth;
      var moduleWidth = obj.width();
      var cardsAmount = 0;
      var cards = obj.find('.xray-card');

      if (stageWidth < breakpoints.small.size) {
        cardsAmount = breakpoints.small.cards
      } else if (stageWidth < breakpoints.medium.size) {
        cardsAmount = breakpoints.medium.cards
      } else {
        cardsAmount = breakpoints.default.cards;
      }

      var cardWidth = moduleWidth / cardsAmount;
      var openCard = null;
      function xrayOpen(card, isRight) {
        var text = card.find('.xray-content p');
        var close = card.find('.xray-close');

        var aniobj = {
          'min-width': cardWidth * 2
        }

        if (isRight) {
          aniobj = {
            'min-width': cardWidth * 2,
            'margin-left': -cardWidth
          }
        }

        card.animate(aniobj, 200, function() {
          close.css('display', 'block');
          close.animate({
            opacity: 1
          }, 100);

          text.show();
          text.animate({
            opacity: 1
          }, 100);

          openCard = card;
        });
      }

      function xrayClose(card) {
        var text = card.find('.xray-content p');
        var close = card.find('.xray-close');

        var aniobj = {
          'min-width': cardWidth,
          'margin-left': 0
        }

        text.animate({
          opacity: 0
        }, 100, function() {
          text.hide();

          close.animate({
            opacity: 0
          }, 100, function() {
            close.css('display', 'none');
          });

          card.animate(aniobj, 200);

          openCard = null;
        });
      }

      cards.unbind();
      cards.on('click', function(e) {
        e.preventDefault();
        var thisCard = $(this);
        if ($(e.target).hasClass('icon-close') || $(e.target).hasClass('xray-close') || thisCard.find('.xray-content p').css('opacity') == 1) {
          xrayClose(thisCard);
        } else if (thisCard != openCard && openCard != null) {
          xrayClose(openCard);
          xrayOpen(thisCard, stageWidth / 2 < e.clientX);
        } else {
          xrayOpen(thisCard, stageWidth / 2 < e.clientX);
        }
      });

      if (xrayCreated == false) {
        xrayCreated = true;
        xraySlider = new Swiper ('.content-module-xray', {
          direction: 'horizontal',
          loop: false,
          slidesPerView: 4,
          spaceBetween: 1,
          pagination: '.swiper-pagination',
          paginationClickable: true,
          breakpoints: {
            480: {
              slidesPerView: 1,
              spaceBetween: 1
            },
            1024: {
              slidesPerView: 2,
              spaceBetween: 1
            }
          }
        });

        xraySlider.on('onSlideChangeStart', function() {
          if (openCard != null) xrayClose(openCard);
        });

        xraySlider.on('onPaginationRendered', function() {
          if (openCard != null) xrayClose(openCard);
        });
      }
    }
  }
  function slickSliders() {
    var slider_defaults = {
      infinite: false,
      dots: true,
      arrows: false
    }

    // TODO refactor this functionality
    // Slider module used for charlie, echo and indiana
    $('.content-module-charlie').slick(slider_defaults);
    $('.content-module-echo').slick(slider_defaults);
    $('.content-module-indiana').slick({
      dots: true,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    $('.content-module-lima, .content-module-zulu').slick({
      dots: true,
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 825,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
    $('.sierra-slider').slick({
      dots: true,
      infinite: false,
      arrows: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    $('.content-module-one').slick({
      dots: true,
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    $('.content-module-whiskey').slick({
      dots: true,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }
  function zuluFilter() {
    $('.zulu-toggle').on('click', function(e){
      e.preventDefault();
      var parent = $(this).parent();
      var buttons = parent.children('.zulu-toggle');
      var data = $(this).data('filter');
      var mod = parent.next();
      if ($(this).hasClass('btn-toggle-active')) {
        $(this).removeClass('btn-toggle-active');
        mod.slick('slickUnfilter');
      } else {
        for (var i = 0; i < buttons.length; i++) {
          $(buttons[i]).removeClass('btn-toggle-active');
        }
        $(this).addClass('btn-toggle-active');
        mod.slick('slickUnfilter');
        mod.slick('slickFilter', '.' + data);
      }
    });
  }
  function whiskeyFunctionality() {
    if (Foundation.MediaQuery.current != 'small' && Foundation.MediaQuery.current != 'medium') {
      $('.whiskey-card').find('h3').css('margin-top', 40);
      $('.whiskey-card').find('.whiskey-text').css('opacity', 0);
      $('.whiskey-card').find('.icon-hover').css('opacity', 1);
      $('.whiskey-card').find('.img-whiskey').css('opacity', 1);

      $('.whiskey-card').on('mouseenter', function() {
        var title = $(this).find('h3');
        var content = $(this).find('.whiskey-text');
        var hover = $(this).find('.icon-hover');
        var img = $(this).find('.img-whiskey');

        title.animate({
          'margin-top': 20
        }, 150);

        content.animate({
          'opacity': 1
        }, 150);

        hover.animate({
          'opacity': 0
        }, 150);

        img.animate({
          'opacity': 0
        }, 150);
      });

      $('.whiskey-card').on('mouseleave', function() {
        var title = $(this).find('h3');
        var content = $(this).find('.whiskey-text');
        var hover = $(this).find('.icon-hover');
        var img = $(this).find('.img-whiskey');

        title.animate({
          'margin-top': 40
        }, 150);

        content.animate({
          'opacity': 0
        }, 150);

        hover.animate({
          'opacity': 1
        }, 150);

        img.animate({
          'opacity': 1
        }, 150);
      });
    } else {
      $('.whiskey-card').unbind();
      $('.whiskey-card').find('h3').css('margin-top', 40);
      $('.whiskey-card').find('.whiskey-text').css('opacity', 1);
      $('.whiskey-card').find('.icon-hover').css('opacity', 0);
      $('.whiskey-card').find('.img-whiskey').css('opacity', 1);
    }
  }
  function androidRenderfix() {
    var anchor = $('.content-module-indiana').find('a');
    anchor.delay(100).css('display', 'block');
    anchor.animate({
      'height': 'auto'
    });
  }
  $(document).foundation();
  $('p').selectionSharer();

  // Checks if media query is reached and runs appropriate functions for new size
  $(window).on('changed.zf.mediaquery', function(){
    mouseEvents();
    xrayFunctionality();
    whiskeyFunctionality();
  });

  $(window).on('replaced.zf.interchange', function() {
    Foundation.reflow(document, 'equalizer');

    slickSliders();
    mouseEvents();
    xrayFunctionality();
    zuluFilter();
    whiskeyFunctionality();
    androidRenderfix();
    iframeRatio('.content-module-kilo');
  });

  slickSliders();
  mouseEvents();
  xrayFunctionality();
  zuluFilter();
  whiskeyFunctionality();
});
